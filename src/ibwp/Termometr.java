package ibwp;

public enum Termometr {
	CIECZOWY("Cieczowy"),
	ELEKTRONICZNY("Elektroniczny");
	
	private String opis;
	
	private Termometr(String opis) {
		this.opis = opis;
	}
	
	public String Opis() {
		return this.opis;
	}
}
