package ibwp;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Klasa przechowująca dane o pomiarze temperatury ciała
 * @author Szymon Sieciński
 */
public class Measurement {
	private int id;
	private String examinedPerson;
	private double temperature;
	private Date measurementDate;
	private Termometr thermometerType;
	
	public Measurement(int id, double temperature, String person, Date measurementDate, Termometr thermometerType) {
		this.id = id;
		this.temperature = temperature;
		this.examinedPerson = person;
		this.measurementDate = measurementDate;
		this.thermometerType = thermometerType;
	}
	
	//Getters and setters
	/**
	 * Zwraca numer identyfikacyjny (ID) obiektu
	 * @return numer identyfikacyjny obiektu
	 */
	public int getID() {
		return id;
	}
	
	/**
	 * Zwraca imię i nazwisko badanej osoby lub jej identyfikator tekstowy.
	 * @return imię i nazwisko lub identyfikator tekstowy
	 */
	public String getExaminedPerson() {
		return examinedPerson;
	}
	public void setExaminedPerson(String examinedPerson) {
		this.examinedPerson = examinedPerson;
	}
	
	/**
	 * Zwraca zapisaną temperaturę w °C.
	 * @return temperatura
	 */
	public double getTemperature() {
		return temperature;
	}
	/**
	 * Zapisuje wartość temperatury w °C.
	 * @param temperature wartość temperatury
	 */
	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}
	/**
	 * Zwraca datę i godzinę wykonania pomiaru.
	 * @return data wykonania pomiaru
	 */
	public Date getMeasurementDate() {
		return measurementDate;
	}
	/**
	 * Zapisuje datę i godzinę wykonania pomiaru.
	 * @param _measurementDate data wykonania pomiaru
	 */
	public void setMeasurementDate(Date _measurementDate) {
		this.measurementDate = _measurementDate;
	}
	/**
	 * Zwraca rodzaj termometru (cieczowy lub elektroniczny).
	 * @return rodzaj termometru
	 */
	public Termometr getTypTermometru() {
		return thermometerType;
	}
	/**
	 * Ustawia rodzaj termometru
	 * @param typTermometru rodzaj termometru
	 */
	public void setTypTermometru(Termometr typTermometru) {
		this.thermometerType = typTermometru;
	}
	
	//Methods
	@Override
	public String toString() {
		return String.format("Pomiar nr %d z dnia %s dla %s: %g (Termometr %s)",
				id,
				new SimpleDateFormat(ibwp.common.Common.DATE_FORMAT).format(measurementDate),
				examinedPerson,
				temperature,
				thermometerType);
	}
}
