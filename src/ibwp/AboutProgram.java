package ibwp;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextPane;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AboutProgram extends JDialog {

	private static final long serialVersionUID = 4641286449506713874L;
	private final JPanel contentPanel = new JPanel();

	/**
	 * Create the dialog.
	 */
	public AboutProgram() {
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setVisible(true);
		
		setTitle("O programie");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new GridLayout(3, 1, 0, 0));
		{
			JLabel lblPomiaryTemperatury = new JLabel("Pomiary Temperatury");
			lblPomiaryTemperatury.setHorizontalAlignment(SwingConstants.CENTER);
			contentPanel.add(lblPomiaryTemperatury);
		}
		{
			JLabel lblSzymon = new JLabel("© 2015 Szymon Sieciński, Dariusz Grabacz");
			lblSzymon.setHorizontalAlignment(SwingConstants.CENTER);
			lblSzymon.setFont(new Font("Dialog", Font.PLAIN, 12));
			contentPanel.add(lblSzymon);
		}
		{
			JTextPane txtpnProgramOpracowanyDo = new JTextPane();
			txtpnProgramOpracowanyDo.setEditable(false);
			txtpnProgramOpracowanyDo.setText("Program opracowany do zbierania pomiarów temperatury ciała i wyświetlania wykresów w ramach projektu z Inżynierii Biomedycznej w Praktyce.");
			contentPanel.add(txtpnProgramOpracowanyDo);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						AboutProgram.this.setVisible(false);
						AboutProgram.this.dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
	}

}
