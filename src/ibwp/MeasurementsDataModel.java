package ibwp;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Date;

/**
 * Implementuje model danych tabeli dla widżetu/kontrolki JTable
 * @author Szymon Sieciński
 */
public class MeasurementsDataModel extends AbstractTableModel {

	private static final long serialVersionUID = -5703139060253754762L;

	private ArrayList<Measurement> measurements;
	private String[] columnNames = {"Lp", "Data i godzina", "Badany", "Temperatura", "Rodzaj termometru"};
	private Class<?>[] columnClasses = {Integer.class, Date.class, String.class, Double.class, Termometr.class};
	
	/**
	 * Tworzy obiekt przechowujący model danych tabeli
	 * @param measurements lista pomiarów
	 */
	public MeasurementsDataModel(ArrayList<Measurement> measurements) {
		this.measurements = new ArrayList<>(measurements);
		this.fireTableDataChanged();
	}
	
	/**
	 * Zwraca liczbę kolumn w modelu.
	 * @return liczba kolumn (5)
	 */
	@Override
	public int getColumnCount() {
		return 5;
	}

	/**
	 * Zwraca liczbę wierszy w modelu.
	 * @return liczba wierszy
	 */
	@Override
	public int getRowCount() {
		return measurements.size();
	}

	/**
	 * Zwraca klasę (typ) kolumny.
	 * @param columnIndex indeks kolumny
	 * @return klasa kolumny
	 */
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return columnClasses[columnIndex];
	}

	/**
	 * Zwraca nazwę kolumny
	 * @param column indeks kolumny
	 * @return nazwa kolumny
	 */
	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	}
	
	/**
	 * Dodaje pomiar do tabeli pomiarów. Dodanie pomiaru do modelu
	 * powoduje wywołanie zdarzenia {@link #fireTableRowsInserted(int firstRow, int lastRow)}.
	 * @param measurement obiekt pomiaru
	 */
	public void addMeasurement(Measurement measurement) {
		measurements.add(measurement);
		this.fireTableRowsInserted(measurements.size(), measurements.size());
	}
	
	/**
	 * Zwraca listę pomiarów.
	 * @return lista pomiarów
	 */
	public ArrayList<Measurement> getMeasurementsList() {
		return measurements;
	}
	
	/**
	 * Usuwa wpisy na liście pomiarów.
	 */
	public void clearMeasurementsList() {
		measurements.clear();
	}

	/**
	 * Zwraca wartość danej komórki tabeli (modelu danych) pomiarów
	 * @param row indeks wiersza
	 * @param column indeks kolumny
	 * @return wartość komórki
	 */
	@Override
	public Object getValueAt(int row, int column) {
		Measurement _measurement = measurements.get(row);
		Object value = null;
		
		switch(column) {
		case 0:
			value = _measurement.getID();
			break;
		case 1:
			value = _measurement.getMeasurementDate();
			break;
		case 2:
			value = _measurement.getExaminedPerson();
			break;
		case 3:
			value = _measurement.getTemperature();
			break;
		case 4:
			value = _measurement.getTypTermometru();
			break;
		}
		
		return value;
	}

}
