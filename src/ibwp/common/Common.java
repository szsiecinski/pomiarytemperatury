package ibwp.common;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import ibwp.Measurement;

import org.knowm.xchart.*;

public class Common {
	public static final String DATE_FORMAT = "dd.MM.yyyy HH:mm:ss";
	
	/**
	 * Serializuje listę pomiarów do pliku JSON.
	 * @param object serializowana lista
	 * @param filename nazwa pliku
	 * @throws Exception
	 */
	public static void Serialize(ArrayList<ibwp.Measurement> object, String filename) throws Exception {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		Type listType = new TypeToken<ArrayList<ibwp.Measurement>>() {}.getType();
		String jsonString = gson.toJson(object, listType);
		
		FileOutputStream fileOutput = new FileOutputStream(filename);
		OutputStreamWriter output = new OutputStreamWriter(fileOutput);
		output.append(jsonString);
		output.close();
	}
	
	/**
	 * Deserializuje wczytany plik JSON do listy pomiarów temperatury
	 * @param jsonStream strumień pliku JSON
	 * @return zdeserializowana lista
	 */
	public static ArrayList<ibwp.Measurement> Deserialize(Reader jsonStream) {
		Type listType = new TypeToken<ArrayList<ibwp.Measurement>>() {}.getType();
		return new Gson().fromJson(jsonStream, listType);
	}
	
	/**
	 * Wyświetla w oknie wykres zależności temperatury od czasu
	 * @param measurements lista pomiarów
	 */
	public static void DrawChart(ArrayList<Measurement> measurements) {
		
		Collections.sort(measurements, new Comparator<Measurement>() {
			  public int compare(Measurement o1, Measurement o2) {
			      return o1.getMeasurementDate().compareTo(o2.getMeasurementDate());
			  }
			});
		
		class DataSeries {
			public ArrayList<Date> dates = new ArrayList<Date>();
			public ArrayList<Double> temperatures = new ArrayList<Double>();
		}
		
		DataSeries cyfrowy = new DataSeries();
		DataSeries cieczowy = new DataSeries();
		
		for(Measurement m : measurements) {
			if(m.getTypTermometru().equals(ibwp.Termometr.CIECZOWY)) {
				cieczowy.dates.add(m.getMeasurementDate());
				cieczowy.temperatures.add(m.getTemperature());
			} else {
				cyfrowy.dates.add(m.getMeasurementDate());
				cyfrowy.temperatures.add(m.getTemperature());
			}
		}
		
		Chart chart = new Chart(640,480);
		chart.setChartTitle("Zmiana temperatury w czasie");
	    chart.setXAxisTitle("Data i godzina");
	    chart.setYAxisTitle("Temperatura w °C");
	    
		chart.addSeries("Termometr cieczowy", cieczowy.dates, cieczowy.temperatures);
		chart.addSeries("Termometr elektroniczny", cyfrowy.dates, cyfrowy.temperatures);
		
		SwingWrapper chartWindow = new ChartWindow(chart);
		chartWindow.displayChart("Wykres");
	}
}
