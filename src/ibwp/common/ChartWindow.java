package ibwp.common;

import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.knowm.xchart.Chart;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XChartPanel;

/**
 * Implementuje zachowanie okna wykresu.
 * @author Szymon Sieciński
 */
public class ChartWindow extends SwingWrapper {
	
	private ArrayList<Chart> charts = new ArrayList<Chart>();
	
	/**
	 * Tworzy obiekt okna wykresu.
	 * @param chart obiekt wykresu danych
	 */
	public ChartWindow(Chart chart) {
		super(chart);
		charts.add(chart);
	}

	/**
	 * Wyświetla wykres w oknie o wskazanym tytule. Modyfikacja metody displayChart
	 * klasy {@link SwingWrapper}.
	 * @param windowTitle tytuł okna
	 */
	@Override
	public JFrame displayChart(String windowTitle) {
		
		//Utworz okno
		final JFrame frame = new JFrame(windowTitle);

	    // Uruchom okno w osobnym wątku
	    javax.swing.SwingUtilities.invokeLater(new Runnable() {

	      @Override
	      public void run() {

	        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	        JPanel chartPanel = new XChartPanel(charts.get(0));
	        frame.add(chartPanel);

	        // Display the window.
	        frame.pack();
	        frame.setVisible(true);
	      }
	    });

	    return frame;
	}
}
