package ibwp;

import java.awt.EventQueue;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.filechooser.*;
import java.util.*;
import java.io.*;
import java.text.*;
import ibwp.common.Common;

public class Window {

	private JFrame frmPomiaryTemperatury;
	private JTextField textField_temperatura;
	private JTextField textField_osoba;
	private JComboBox<String> comboBoxExamined;
	private final JSpinner spinner_data = new JSpinner();
	private final JComboBox<Termometr> comboBoxTermometr = new JComboBox<Termometr>();
	
	private JTable table;
	private int id = 1;

	private ArrayList<Measurement> measurements;
	private MeasurementsDataModel dataModel;
	private Map<String, ArrayList<Measurement>> groupedMeasurements = new HashMap<String, ArrayList<Measurement>>();

	/**
	 * Obsługuje otwarcie pliku. Otwarcie pliku jest wykonywane w następujących krokach:
	 * 1. utworzenie okna dialogowego otwierania pliku o tytule "Otwórz plik rejestru"
	 * z filtrami „Pliki JSON” i „Wszystkie”;
	 * 2. otwarcie okna dialogowego i zwrócenie stanu
	 * 3. obsługa stanu zaakceptowania (otwórz).
	 * @author Szymon Sieciński
	 */
	protected class OpenFile implements ActionListener {
		
		/**
		 * Wykonuje się po wystąpieniu zdarzenia.
		 * @param e zdarzenie
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			final JFileChooser fileOpener = new JFileChooser();
			fileOpener.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			fileOpener.setMultiSelectionEnabled(false);
			FileNameExtensionFilter filter = new FileNameExtensionFilter("Pliki JSON", "json");
			fileOpener.addChoosableFileFilter(filter);
			fileOpener.setFileFilter(filter);
			fileOpener.setDialogTitle("Otwórz plik rejestru");

			int result = fileOpener.showOpenDialog(frmPomiaryTemperatury);
			if (result == JFileChooser.APPROVE_OPTION) {
				File selectedFile = fileOpener.getSelectedFile();
				FileInputStream inputStream;
				try {
					inputStream = new FileInputStream(selectedFile);
					BufferedReader selectedFileReader = new BufferedReader(new InputStreamReader(inputStream));

					measurements = new ArrayList<Measurement>(Common.Deserialize(selectedFileReader));
					dataModel.clearMeasurementsList();
					for (Measurement m : measurements) {
						dataModel.addMeasurement(m);
					}
					groupByPersonName();
					setPatientComboBox(comboBoxExamined);
					
					Measurement maxIDMeasurement = Collections.max(measurements, new Comparator<Measurement>() {

						@Override
						public int compare(Measurement o1, Measurement o2) {
							if (o1.getID() == o2.getID()) {
								return 0;
							} else if (o1.getID() > o2.getID()) {
								return 1;
							} else {
								return -1;
							}
						}
					});

					id = maxIDMeasurement.getID()+1;


				} catch (FileNotFoundException e1) {
					JOptionPane.showMessageDialog(frmPomiaryTemperatury, "Nie można odczytać pliku.", "Błąd",
							JOptionPane.ERROR_MESSAGE);
					e1.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Obsługuje zapisywanie pliku. Zapisanie pliku jest wykonywane w następujących krokach:
	 * 1. utworzenie okna dialogowego zapisu pliku o tytule "Zapisz plik rejestru"
	 * z filtrami „Pliki JSON” i „Wszystkie”;
	 * 2. otwarcie okna dialogowego i zwrócenie stanu
	 * 3. obsługa stanu zaakceptowania (zapisz).
	 * @author Szymon Sieciński
	 */
	protected class SaveFile implements ActionListener {

		/**
		 * Wykonuje się po wystąpieniu zdarzenia.
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			final JFileChooser fileSaver = new JFileChooser();
			fileSaver.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			fileSaver.setMultiSelectionEnabled(false);
			FileNameExtensionFilter filter = new FileNameExtensionFilter("Pliki JSON", "json");
			fileSaver.addChoosableFileFilter(filter);
			fileSaver.setFileFilter(filter);
			fileSaver.setDialogTitle("Zapisz plik rejestru");

			int result = fileSaver.showSaveDialog(frmPomiaryTemperatury);
			if (result == JFileChooser.APPROVE_OPTION) {
				File selectedFile = fileSaver.getSelectedFile();
				String filename = selectedFile.getAbsolutePath();
				
				if(!filename.endsWith(".json")) filename.concat(".json");
				
				try {
					Common.Serialize(measurements, filename);
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(frmPomiaryTemperatury, "Nie można zapisać pliku.", "Błąd",
							JOptionPane.ERROR_MESSAGE);
					e1.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Pokazuje wykres zmiany temperatury dla wszystkich dostępnych pomiarów
	 * @author Szymon Sieciński
	 */
	protected class ShowChart implements ActionListener {
		
		/**
		 * Wykonuje się po wystąpieniu zdarzenia.
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				Common.DrawChart(measurements);
			} catch(Exception ex) {
				showChartErrorDialog();
			}
		}
		
	}
	
	/**
	 * Pokazuje wykres zmiany temperatury dla wybranego użytkownika
	 * @author Szymon Sieciński
	 */
	protected class ShowChartOfPerson implements ActionListener {
		
		/**
		 * Wykonuje się po wystąpieniu zdarzenia.
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				String personName = comboBoxExamined.getSelectedItem().toString();
				ArrayList<Measurement> measurementsForPerson = groupedMeasurements.get(personName);
				Common.DrawChart(measurementsForPerson);
				
			} catch(Exception ex) {
				showChartErrorDialog();
			}
		}
		
	}
	
	/**
	 * Pokazuje okno O programie.
	 */
	protected class ShowAboutDialog implements ActionListener {
		
		/**
		 * Wykonuje się po wystąpieniu zdarzenia.
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			AboutProgram aboutWindow = new AboutProgram();
			aboutWindow.setVisible(true);
		}
		
	}
	
	/**
	 * Zamyka okno i kończy pracę programu.
	 */
	protected class Quit implements ActionListener {

		/**
		 * Wykonuje się po wystąpieniu zdarzenia.
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			closeWindow();
		}
	}
	
	/**
	 * Dodaje pozycję do listy pomiarów.
	 */
	protected class AddPosition implements ActionListener {

		/**
		 * Wykonuje się po wystąpieniu zdarzenia.
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			String badany = textField_osoba.getText();
			// String zapisana_data = spinner_data.getValue().toString();
			java.text.SimpleDateFormat dateFormat = new SimpleDateFormat(Common.DATE_FORMAT);
			String zapisana_data = dateFormat.format(spinner_data.getValue());
			Date data_badania = null;

			try {
				data_badania = dateFormat.parse(zapisana_data);
			} catch (ParseException e1) {
				JOptionPane.showMessageDialog(frmPomiaryTemperatury, "Błąd parsowania daty.", "Błąd",
						JOptionPane.ERROR_MESSAGE);
				e1.printStackTrace();
			}

			double temperatura = 36.6;
			Termometr typ_termometru = (Termometr) comboBoxTermometr.getSelectedItem();

			try {
				temperatura = Double.parseDouble(textField_temperatura.getText());
				Measurement measurement = new Measurement(id, temperatura, badany, data_badania, typ_termometru);
				dataModel.addMeasurement(measurement);
				measurements = new ArrayList<Measurement>(dataModel.getMeasurementsList());

				spinner_data.setValue(new Date());
				id++;
			} catch (NumberFormatException ex) {
				JOptionPane.showMessageDialog(frmPomiaryTemperatury, "Podaj wartość liczbową temperatury.", "Błąd",
						JOptionPane.ERROR_MESSAGE);
				ex.printStackTrace();
			}
		}
	}
	
	/**
	 * Uruchamia okno programu.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window window = new Window();
					window.frmPomiaryTemperatury.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Tworzy okno programu.
	 */
	public Window() {
		initialize();
	}

	/**
	 * Grupuje pomiary temperatury według badanych osób.
	 */
	protected void groupByPersonName() {
		
		for(Measurement measurement : measurements) {
			String patient = measurement.getExaminedPerson();
			if(groupedMeasurements.get(patient) == null) {
				groupedMeasurements.put(patient, new ArrayList<Measurement>());
			} else {
				groupedMeasurements.get(patient).add(measurement);
			}
		}
	}
	
	private void setPatientComboBox(JComboBox<String> comboBox) {
		comboBox.setModel(new DefaultComboBoxModel(groupedMeasurements.keySet().toArray()));
	}
	
	/**
	 * Pokazuje informację o błędzie.
	 */
	protected void showChartErrorDialog() {
			JOptionPane.showMessageDialog(frmPomiaryTemperatury, "Przed wyświetleniem wykresu należy wczytać dane", "Błąd", JOptionPane.ERROR_MESSAGE);
	}
	
	/**
	 * Inicjalizuje zawartość okna.
	 */
	private void initialize() {
		measurements = new ArrayList<Measurement>();
		dataModel = new MeasurementsDataModel(measurements);

		frmPomiaryTemperatury = new JFrame();
		frmPomiaryTemperatury.setTitle("Pomiary temperatury");
		frmPomiaryTemperatury.setBounds(100, 100, 621, 418);
		frmPomiaryTemperatury.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JMenuBar menuBar = new JMenuBar();
		frmPomiaryTemperatury.setJMenuBar(menuBar);

		JMenu mnPlik = new JMenu("Plik");
		menuBar.add(mnPlik);

		JMenuItem mntmOtwrzRejestr = new JMenuItem("Otwórz rejestr");
		mntmOtwrzRejestr.addActionListener(new OpenFile());
		mnPlik.add(mntmOtwrzRejestr);

		JMenuItem mntmZapiszRejestr = new JMenuItem("Zapisz rejestr");
		mntmZapiszRejestr.addActionListener(new SaveFile());
		mnPlik.add(mntmZapiszRejestr);

		JSeparator separator = new JSeparator();
		mnPlik.add(separator);

		JMenuItem mntmZakocz = new JMenuItem("Zakończ");
		mntmZakocz.addActionListener(new Quit());
		mnPlik.add(mntmZakocz);
		
		JMenu mnNarzdzia = new JMenu("Narzędzia");
		menuBar.add(mnNarzdzia);
		
		JMenuItem mntmWykres = new JMenuItem("Wykres");
		mntmWykres.addActionListener(new ShowChart());
		mnNarzdzia.add(mntmWykres);
		
		JMenuItem mntmWykresDlaBadanego = new JMenuItem("Wykres dla badanego");
		mntmWykresDlaBadanego.addActionListener(new ShowChartOfPerson());
		mnNarzdzia.add(mntmWykresDlaBadanego);

		JMenu mnPomoc = new JMenu("Pomoc");
		menuBar.add(mnPomoc);

		JMenuItem mntmOProgramie = new JMenuItem("O programie");
		mntmOProgramie.addActionListener(new ShowAboutDialog());
		mnPomoc.add(mntmOProgramie);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 1.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		frmPomiaryTemperatury.getContentPane().setLayout(gridBagLayout);

		JLabel lblTemperatura = new JLabel("Temperatura w °C");
		GridBagConstraints gbc_lblTemperatura = new GridBagConstraints();
		gbc_lblTemperatura.insets = new Insets(0, 0, 5, 5);
		gbc_lblTemperatura.gridx = 0;
		gbc_lblTemperatura.gridy = 0;
		frmPomiaryTemperatury.getContentPane().add(lblTemperatura, gbc_lblTemperatura);

		textField_temperatura = new JTextField();
		GridBagConstraints gbc_textField_temperatura = new GridBagConstraints();
		gbc_textField_temperatura.insets = new Insets(0, 0, 5, 5);
		gbc_textField_temperatura.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_temperatura.gridx = 1;
		gbc_textField_temperatura.gridy = 0;
		frmPomiaryTemperatury.getContentPane().add(textField_temperatura, gbc_textField_temperatura);
		textField_temperatura.setColumns(10);

		JLabel lblBadany = new JLabel("Badany");
		lblBadany.setHorizontalAlignment(SwingConstants.TRAILING);
		GridBagConstraints gbc_lblBadany = new GridBagConstraints();
		gbc_lblBadany.insets = new Insets(0, 0, 5, 5);
		gbc_lblBadany.gridx = 0;
		gbc_lblBadany.gridy = 1;
		frmPomiaryTemperatury.getContentPane().add(lblBadany, gbc_lblBadany);

		textField_osoba = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 1;
		frmPomiaryTemperatury.getContentPane().add(textField_osoba, gbc_textField);
		textField_osoba.setColumns(10);

		JLabel lblData = new JLabel("Data");
		GridBagConstraints gbc_lblData = new GridBagConstraints();
		gbc_lblData.insets = new Insets(0, 0, 5, 5);
		gbc_lblData.gridx = 0;
		gbc_lblData.gridy = 2;
		frmPomiaryTemperatury.getContentPane().add(lblData, gbc_lblData);

		spinner_data.setModel(new SpinnerDateModel(new Date(), null, null, Calendar.SECOND));
		spinner_data.setEditor(new JSpinner.DateEditor(spinner_data, Common.DATE_FORMAT));
		GridBagConstraints gbc_spinner_data = new GridBagConstraints();
		gbc_spinner_data.insets = new Insets(0, 0, 5, 5);
		gbc_spinner_data.gridx = 1;
		gbc_spinner_data.gridy = 2;
		frmPomiaryTemperatury.getContentPane().add(spinner_data, gbc_spinner_data);

		JLabel lblTermometr = new JLabel("Termometr");
		GridBagConstraints gbc_lblTermometr = new GridBagConstraints();
		gbc_lblTermometr.anchor = GridBagConstraints.EAST;
		gbc_lblTermometr.insets = new Insets(0, 0, 5, 5);
		gbc_lblTermometr.gridx = 0;
		gbc_lblTermometr.gridy = 3;
		frmPomiaryTemperatury.getContentPane().add(lblTermometr, gbc_lblTermometr);

		comboBoxTermometr.setModel(new DefaultComboBoxModel<Termometr>(Termometr.values()));
		GridBagConstraints gbc_comboBoxTermometr = new GridBagConstraints();
		gbc_comboBoxTermometr.insets = new Insets(0, 0, 5, 5);
		gbc_comboBoxTermometr.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBoxTermometr.gridx = 1;
		gbc_comboBoxTermometr.gridy = 3;
		frmPomiaryTemperatury.getContentPane().add(comboBoxTermometr, gbc_comboBoxTermometr);
		
		JButton btnDodajPozycje = new JButton("Dodaj pozycję");
		btnDodajPozycje.addActionListener(new AddPosition());
		
		JLabel lblBadanyNaWykresie = new JLabel("Badany na wykresie");	
		GridBagConstraints gbc_lblBadanyNaWykresie = new GridBagConstraints();
		gbc_lblBadanyNaWykresie.insets = new Insets(0, 0, 5, 5);
		gbc_lblBadanyNaWykresie.gridx = 0;
		gbc_lblBadanyNaWykresie.gridy = 4;
		frmPomiaryTemperatury.getContentPane().add(lblBadanyNaWykresie, gbc_lblBadanyNaWykresie);
		
		comboBoxExamined = new JComboBox<String>();		
		GridBagConstraints gbc_comboBoxExamined = new GridBagConstraints();
		gbc_comboBoxExamined.insets = new Insets(0, 0, 5, 5);
		gbc_comboBoxExamined.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBoxExamined.gridx = 0;
		gbc_comboBoxExamined.gridy = 5;
		frmPomiaryTemperatury.getContentPane().add(comboBoxExamined, gbc_comboBoxExamined);
		GridBagConstraints gbc_btnDodajPozycje = new GridBagConstraints();
		gbc_btnDodajPozycje.insets = new Insets(0, 0, 5, 5);
		gbc_btnDodajPozycje.gridx = 1;
		gbc_btnDodajPozycje.gridy = 5;
		frmPomiaryTemperatury.getContentPane().add(btnDodajPozycje, gbc_btnDodajPozycje);

		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridwidth = 3;
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 6;
		frmPomiaryTemperatury.getContentPane().add(scrollPane, gbc_scrollPane);

		table = new JTable();
		table.setModel(dataModel);
		scrollPane.setViewportView(table);
	}

	/**
	 * Zamyka okno programu.
	 */
	protected void closeWindow() {
		frmPomiaryTemperatury.setVisible(false);
		frmPomiaryTemperatury.dispose();
	}
}
